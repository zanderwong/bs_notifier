// +build wireinject

// The build tag makes sure the stub is not built in the final build.

package main

import (
	"github.com/go-kratos/kratos/v2"
	"github.com/go-kratos/kratos/v2/log"
	"github.com/google/wire"
	"gitlab.com/zanderwong/bs_notifier/internal/biz"
	"gitlab.com/zanderwong/bs_notifier/internal/conf"
	"gitlab.com/zanderwong/bs_notifier/internal/data"
	"gitlab.com/zanderwong/bs_notifier/internal/server"
	"gitlab.com/zanderwong/bs_notifier/internal/service"
)

// wireApp init kratos application.
func wireApp(*conf.Server, *conf.Data, log.Logger) (*kratos.App, func(), error) {
	panic(wire.Build(server.ProviderSet, data.ProviderSet, biz.ProviderSet, service.ProviderSet, newApp))
}
