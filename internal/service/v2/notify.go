package v2

import (
	"context"
	"net/http"

	pb "gitlab.com/zanderwong/bs_notifier/api/notify/v2"
	"gitlab.com/zanderwong/bs_notifier/internal/biz"
)

type NotifyService struct {
	pb.UnimplementedNotifyServer
	uc *biz.NotifyV2Usecase
}

func NewNotifyService(uc *biz.NotifyV2Usecase) *NotifyService {
	return &NotifyService{uc: uc}
}

func (s *NotifyService) CreateNotify(ctx context.Context, req *pb.CreateNotifyRequest) (*pb.CreateNotifyReply, error) {
	if _, err := s.uc.CreateNotifyV2(ctx, &biz.NotifyV2{}); err != nil {
		return &pb.CreateNotifyReply{
			Code:    http.StatusInternalServerError,
			Data:    nil,
			Message: biz.ErrNotifyV1InternalServerError.Message,
		}, nil
	}
	return &pb.CreateNotifyReply{Code: http.StatusOK, Data: nil, Message: "succeed"}, nil
}
func (s *NotifyService) UpdateNotify(ctx context.Context, req *pb.UpdateNotifyRequest) (*pb.UpdateNotifyReply, error) {
	return &pb.UpdateNotifyReply{}, nil
}
func (s *NotifyService) DeleteNotify(ctx context.Context, req *pb.DeleteNotifyRequest) (*pb.DeleteNotifyReply, error) {
	return &pb.DeleteNotifyReply{}, nil
}
func (s *NotifyService) GetNotify(ctx context.Context, req *pb.GetNotifyRequest) (*pb.GetNotifyReply, error) {
	return &pb.GetNotifyReply{}, nil
}
func (s *NotifyService) ListNotify(ctx context.Context, req *pb.ListNotifyRequest) (*pb.ListNotifyReply, error) {
	return &pb.ListNotifyReply{}, nil
}

func (s *NotifyService) CreateToDoJob(ctx context.Context, req *pb.CreateToDoJobRequest) (*pb.CreateToDoJobReply, error) {
	if _, err := s.uc.CreateToDoJob(ctx, &biz.NotifyV2{}); err != nil {
		return &pb.CreateToDoJobReply{
			Code:    http.StatusInternalServerError,
			Data:    nil,
			Message: biz.ErrNotifyV1InternalServerError.Message,
		}, nil
	}
	return &pb.CreateToDoJobReply{Code: http.StatusOK, Data: nil, Message: "succeed"}, nil
}

func (s *NotifyService) DeleteToDoJob(ctx context.Context, req *pb.DeleteToDoJobRequest) (*pb.DeleteToDoJobReply, error) {
	if _, err := s.uc.DeleteToDoJob(ctx, req.MessageId); err != nil {
		return &pb.DeleteToDoJobReply{
			Code:    http.StatusInternalServerError,
			Data:    nil,
			Message: biz.ErrNotifyV1InternalServerError.Message,
		}, nil
	}
	return &pb.DeleteToDoJobReply{Code: http.StatusOK, Data: nil, Message: "succeed"}, nil
}
