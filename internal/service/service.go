package service

import (
	"github.com/google/wire"
	v1 "gitlab.com/zanderwong/bs_notifier/internal/service/v1"
	v2 "gitlab.com/zanderwong/bs_notifier/internal/service/v2"
)

// ProviderSet is service providers.
var ProviderSet = wire.NewSet(NewGreeterService, v1.NewNotifyService, v2.NewNotifyService)
