package v1

import (
	"context"
	"net/http"

	pb "gitlab.com/zanderwong/bs_notifier/api/notify/v1"
	"gitlab.com/zanderwong/bs_notifier/internal/biz"
)

type NotifyService struct {
	pb.UnimplementedNotifyServer
	uc *biz.NotifyV1Usecase
}

func NewNotifyService(uc *biz.NotifyV1Usecase) *NotifyService {
	return &NotifyService{uc: uc}
}

func (s *NotifyService) CreateNotify(ctx context.Context, req *pb.CreateNotifyRequest) (*pb.CreateNotifyReply, error) {
	s.uc.CreateNotifyV1(ctx, &biz.NotifyV1{})
	return &pb.CreateNotifyReply{
		Code:    http.StatusOK,
		Data:    nil,
		Message: "succeed",
	}, nil
}
func (s *NotifyService) UpdateNotify(ctx context.Context, req *pb.UpdateNotifyRequest) (*pb.UpdateNotifyReply, error) {
	return &pb.UpdateNotifyReply{}, nil
}
func (s *NotifyService) DeleteNotify(ctx context.Context, req *pb.DeleteNotifyRequest) (*pb.DeleteNotifyReply, error) {
	return &pb.DeleteNotifyReply{}, nil
}
func (s *NotifyService) GetNotify(ctx context.Context, req *pb.GetNotifyRequest) (*pb.GetNotifyReply, error) {
	n, err := s.uc.GetNotifyV1(ctx, &biz.NotifyV1{Id: req.Id})
	if err != nil {
		return &pb.GetNotifyReply{Code: http.StatusInternalServerError, Data: nil, Message: biz.ErrNotifyV1InternalServerError.Message}, nil
	}
	if n.Id < 0 {
		return &pb.GetNotifyReply{Code: http.StatusNotFound, Data: nil, Message: biz.ErrNotifyV1NotFound.Message}, nil
	}
	return &pb.GetNotifyReply{Code: http.StatusOK, Data: nil, Message: "succeed"}, nil
}
func (s *NotifyService) ListNotify(ctx context.Context, req *pb.ListNotifyRequest) (*pb.ListNotifyReply, error) {
	return &pb.ListNotifyReply{}, nil
}
func (s *NotifyService) CreateTaskJob(ctx context.Context, req *pb.CreateTaskJobRequest) (*pb.CreateTaskJobReply, error) {
	if _, err := s.uc.CreateTaskJob(ctx, &biz.NotifyV1{}); err != nil {
		return &pb.CreateTaskJobReply{Code: http.StatusInternalServerError, Data: nil, Message: biz.ErrNotifyV1InternalServerError.Message}, nil
	}
	return &pb.CreateTaskJobReply{
		Code:    http.StatusOK,
		Data:    nil,
		Message: "succeed",
	}, nil
}
func (s *NotifyService) DeleteTaskJob(ctx context.Context, req *pb.DeleteTaskJobRequest) (*pb.DeleteTaskJobReply, error) {
	if _, err := s.uc.DeleteTaskJob(ctx, &biz.NotifyV1{}); err != nil {
		return &pb.DeleteTaskJobReply{Code: http.StatusInternalServerError, Data: nil, Message: biz.ErrNotifyV1InternalServerError.Message}, nil
	}
	return &pb.DeleteTaskJobReply{
		Code:    http.StatusOK,
		Data:    nil,
		Message: "succeed",
	}, nil
}
