package data

import (
	"context"

	"github.com/go-kratos/kratos/v2/log"
	"gitlab.com/zanderwong/bs_notifier/internal/biz"
)

type notifyV2Repo struct {
	data *Data
	log  *log.Helper
}

// NewNotifyV2Repo .
func NewNotifyV2Repo(data *Data, logger log.Logger) biz.NotifyV2Repo {
	return &notifyV2Repo{
		data: data,
		log:  log.NewHelper(logger),
	}
}

func (r *notifyV2Repo) Create(ctx context.Context, g *biz.NotifyV2) (*biz.NotifyV2, error) {
	return g, nil
}

func (r *notifyV2Repo) Update(ctx context.Context, g *biz.NotifyV2) (*biz.NotifyV2, error) {
	return g, nil
}

func (r *notifyV2Repo) FindByID(context.Context, int64) (*biz.NotifyV2, error) {
	return nil, nil
}

func (r *notifyV2Repo) DeleteByID(context.Context, int64) (*biz.NotifyV2, error) {
	return nil, nil
}

func (r *notifyV2Repo) ListAll(context.Context) ([]*biz.NotifyV2, error) {
	return nil, nil
}

func (r *notifyV2Repo) CreateJob(ctx context.Context, g *biz.NotifyV2) (*biz.NotifyV2, error) {
	return g, nil
}

func (r *notifyV2Repo) DeleteJobByID(context.Context, string) (*biz.NotifyV2, error) {
	return nil, nil
}
