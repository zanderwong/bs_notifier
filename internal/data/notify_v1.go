package data

import (
	"context"

	"github.com/go-kratos/kratos/v2/log"
	"gitlab.com/zanderwong/bs_notifier/internal/biz"
)

type notifyV1Repo struct {
	data *Data
	log  *log.Helper
}

// NewNotifyV1Repo .
func NewNotifyV1Repo(data *Data, logger log.Logger) biz.NotifyV1Repo {
	return &notifyV1Repo{
		data: data,
		log:  log.NewHelper(logger),
	}
}

func (r *notifyV1Repo) Create(ctx context.Context, g *biz.NotifyV1) (*biz.NotifyV1, error) {
	return g, nil
}

func (r *notifyV1Repo) Update(ctx context.Context, g *biz.NotifyV1) (*biz.NotifyV1, error) {
	return g, nil
}

func (r *notifyV1Repo) FindByID(context.Context, uint64) (*biz.NotifyV1, error) {
	return &biz.NotifyV1{}, nil
}

func (r *notifyV1Repo) DeleteByID(context.Context, uint64) (*biz.NotifyV1, error) {
	return nil, nil
}

func (r *notifyV1Repo) ListAll(context.Context) ([]*biz.NotifyV1, error) {
	return nil, nil
}

func (r *notifyV1Repo) CreateJob(ctx context.Context, g *biz.NotifyV1) (*biz.NotifyV1, error) {
	return g, nil
}

func (r *notifyV1Repo) DeleteJobByID(context.Context, string) (*biz.NotifyV1, error) {
	return nil, nil
}
