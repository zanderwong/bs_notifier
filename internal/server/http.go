package server

import (
	"github.com/go-kratos/kratos/v2/log"
	"github.com/go-kratos/kratos/v2/middleware/recovery"
	"github.com/go-kratos/kratos/v2/middleware/validate"
	"github.com/go-kratos/kratos/v2/transport/http"
	v1 "gitlab.com/zanderwong/bs_notifier/api/helloworld/v1"
	apiV1 "gitlab.com/zanderwong/bs_notifier/api/notify/v1"
	apiV2 "gitlab.com/zanderwong/bs_notifier/api/notify/v2"
	"gitlab.com/zanderwong/bs_notifier/internal/conf"
	"gitlab.com/zanderwong/bs_notifier/internal/service"
	nsV1 "gitlab.com/zanderwong/bs_notifier/internal/service/v1"
	nsV2 "gitlab.com/zanderwong/bs_notifier/internal/service/v2"
)

// NewHTTPServer new a HTTP server.
func NewHTTPServer(c *conf.Server, greeter *service.GreeterService, notifyV1 *nsV1.NotifyService, notifyV2 *nsV2.NotifyService, logger log.Logger) *http.Server {
	var opts = []http.ServerOption{
		http.Middleware(
			recovery.Recovery(),
			validate.Validator(),
		),
	}
	if c.Http.Network != "" {
		opts = append(opts, http.Network(c.Http.Network))
	}
	if c.Http.Addr != "" {
		opts = append(opts, http.Address(c.Http.Addr))
	}
	if c.Http.Timeout != nil {
		opts = append(opts, http.Timeout(c.Http.Timeout.AsDuration()))
	}
	srv := http.NewServer(opts...)
	v1.RegisterGreeterHTTPServer(srv, greeter)
	apiV1.RegisterNotifyHTTPServer(srv, notifyV1)
	apiV2.RegisterNotifyHTTPServer(srv, notifyV2)
	return srv
}
