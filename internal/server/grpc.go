package server

import (
	"github.com/go-kratos/kratos/v2/log"
	"github.com/go-kratos/kratos/v2/middleware/recovery"
	"github.com/go-kratos/kratos/v2/middleware/validate"
	"github.com/go-kratos/kratos/v2/transport/grpc"
	v1 "gitlab.com/zanderwong/bs_notifier/api/helloworld/v1"
	apiV1 "gitlab.com/zanderwong/bs_notifier/api/notify/v1"
	apiV2 "gitlab.com/zanderwong/bs_notifier/api/notify/v2"
	"gitlab.com/zanderwong/bs_notifier/internal/conf"
	"gitlab.com/zanderwong/bs_notifier/internal/service"
	nsV1 "gitlab.com/zanderwong/bs_notifier/internal/service/v1"
	nsV2 "gitlab.com/zanderwong/bs_notifier/internal/service/v2"
)

// NewGRPCServer new a gRPC server.
func NewGRPCServer(c *conf.Server, greeter *service.GreeterService, notifyV1 *nsV1.NotifyService, notifyV2 *nsV2.NotifyService, logger log.Logger) *grpc.Server {
	var opts = []grpc.ServerOption{
		grpc.Middleware(
			recovery.Recovery(),
			validate.Validator(),
		),
	}
	if c.Grpc.Network != "" {
		opts = append(opts, grpc.Network(c.Grpc.Network))
	}
	if c.Grpc.Addr != "" {
		opts = append(opts, grpc.Address(c.Grpc.Addr))
	}
	if c.Grpc.Timeout != nil {
		opts = append(opts, grpc.Timeout(c.Grpc.Timeout.AsDuration()))
	}
	srv := grpc.NewServer(opts...)

	v1.RegisterGreeterServer(srv, greeter)
	apiV1.RegisterNotifyServer(srv, notifyV1)
	apiV2.RegisterNotifyServer(srv, notifyV2)

	return srv
}
