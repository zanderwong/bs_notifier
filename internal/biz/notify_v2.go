package biz

import (
	"context"

	v2 "gitlab.com/zanderwong/bs_notifier/api/notify/v2"

	"github.com/go-kratos/kratos/v2/errors"
	"github.com/go-kratos/kratos/v2/log"
)

var (
	// ErrNotifyV2NotFound is notify not found.
	ErrNotifyV2NotFound            = errors.NotFound(v2.ErrorReason_NOTIFY_NOT_FOUND.String(), "notify not found")
	ErrNotifyV2InternalServerError = errors.InternalServer(v2.ErrorReason_NOTIFY_INTERNAL_SERVER_ERROR.String(), "internal server error")
)

// NotifyV2 is a NotifyV2 model.
type NotifyV2 struct {
	Hello string
}

// NotifyV2Repo is a NotifyV2 repo.
type NotifyV2Repo interface {
	Create(context.Context, *NotifyV2) (*NotifyV2, error)
	Update(context.Context, *NotifyV2) (*NotifyV2, error)
	FindByID(context.Context, int64) (*NotifyV2, error)
	DeleteByID(context.Context, int64) (*NotifyV2, error)
	ListAll(context.Context) ([]*NotifyV2, error)
	CreateJob(context.Context, *NotifyV2) (*NotifyV2, error)
	DeleteJobByID(context.Context, string) (*NotifyV2, error)
}

// NotifyV2Usecase is a Greeter usecase.
type NotifyV2Usecase struct {
	repo NotifyV2Repo
	log  *log.Helper
}

// NewNotifyV2Usecase new a Notify usecase.
func NewNotifyV2Usecase(repo NotifyV2Repo, logger log.Logger) *NotifyV2Usecase {
	return &NotifyV2Usecase{repo: repo, log: log.NewHelper(logger)}
}

// CreateNotifyV2 creates a Notify, and returns the new Notify.
func (uc *NotifyV2Usecase) CreateNotifyV2(ctx context.Context, n *NotifyV2) (*NotifyV2, error) {
	uc.log.WithContext(ctx).Infof("NotifyCreate: %v", n.Hello)
	return uc.repo.Create(ctx, n)
}

// CreateToDoJob creates a ToDoJob, and returns the new ToDoJob.
func (uc *NotifyV2Usecase) CreateToDoJob(ctx context.Context, n *NotifyV2) (*NotifyV2, error) {
	uc.log.WithContext(ctx).Info("todo job create request")
	return uc.repo.CreateJob(ctx, n)
}

// DeleteToDoJob delete a ToDoJob, and returns the new ToDoJob.
func (uc *NotifyV2Usecase) DeleteToDoJob(ctx context.Context, todoJobId string) (*NotifyV2, error) {
	uc.log.WithContext(ctx).Info("todo job delete request")
	return uc.repo.DeleteJobByID(ctx, todoJobId)
}
