package biz

import (
	"context"
	"github.com/go-kratos/kratos/v2/errors"
	"github.com/go-kratos/kratos/v2/log"
	v1 "gitlab.com/zanderwong/bs_notifier/api/notify/v1"
)

var (
	// ErrNotifyV1NotFound is notify not found.
	ErrNotifyV1NotFound            = errors.NotFound(v1.ErrorReason_NOTIFY_NOT_FOUND.String(), "notify not found")
	ErrNotifyV1InternalServerError = errors.InternalServer(v1.ErrorReason_NOTIFY_INTERNAL_SERVER_ERROR.String(), "internal server error")
)

// NotifyV1 is a NotifyV1 model.
type NotifyV1 struct {
	Id   uint64
	Task struct {
		TaskId string
	}
}

// NotifyV1Repo is a NotifyV1 repo.
type NotifyV1Repo interface {
	Create(context.Context, *NotifyV1) (*NotifyV1, error)
	Update(context.Context, *NotifyV1) (*NotifyV1, error)
	FindByID(context.Context, uint64) (*NotifyV1, error)
	DeleteByID(context.Context, uint64) (*NotifyV1, error)
	ListAll(context.Context) ([]*NotifyV1, error)
	CreateJob(context.Context, *NotifyV1) (*NotifyV1, error)
	DeleteJobByID(context.Context, string) (*NotifyV1, error)
}

// NotifyV1Usecase is a Greeter usecase.
type NotifyV1Usecase struct {
	repo NotifyV1Repo
	log  *log.Helper
}

// NewNotifyV1Usecase new a Notify usecase.
func NewNotifyV1Usecase(repo NotifyV1Repo, logger log.Logger) *NotifyV1Usecase {
	return &NotifyV1Usecase{repo: repo, log: log.NewHelper(logger)}
}

// CreateNotifyV1 creates a Notify, and returns the new Notify.
func (uc *NotifyV1Usecase) CreateNotifyV1(ctx context.Context, n *NotifyV1) (*NotifyV1, error) {
	uc.log.WithContext(ctx).Info("notify create request")
	return uc.repo.Create(ctx, n)
}

// GetNotifyV1 get a Notify, and returns the new Notify.
func (uc *NotifyV1Usecase) GetNotifyV1(ctx context.Context, n *NotifyV1) (*NotifyV1, error) {
	uc.log.WithContext(ctx).Info("notify get request")
	return uc.repo.FindByID(ctx, n.Id)
}

// CreateTaskJob creates a TaskJob, and returns the new TaskJob.
func (uc *NotifyV1Usecase) CreateTaskJob(ctx context.Context, n *NotifyV1) (*NotifyV1, error) {
	uc.log.WithContext(ctx).Info("task job create request")
	return uc.repo.CreateJob(ctx, n)
}

// DeleteTaskJob delete a TaskJob, and returns the new TaskJob.
func (uc *NotifyV1Usecase) DeleteTaskJob(ctx context.Context, n *NotifyV1) (*NotifyV1, error) {
	uc.log.WithContext(ctx).Info("task job delete request")
	return uc.repo.DeleteJobByID(ctx, n.Task.TaskId)
}
